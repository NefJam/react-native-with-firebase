import React from 'react';
import {StatusBar} from 'react-native';
import firebase from 'react-native-firebase';
import styles  from './../Styles/styles';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Text, List, ListItem, Thumbnail, StyleProvider, Form, Item, Input, Label} from "native-base";
import getTheme from './../../native-base-theme/components';
import material from './../../native-base-theme/variables/material';
import RadialGradient from 'react-native-radial-gradient';

export default class Login extends React.Component {
  state = { email: '', password: '', errorMessage: null }

  handleSignUp = () => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => this.props.navigation.navigate('Main'))
      .catch(error => this.setState({ errorMessage: error.message }))
  }

  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
        <StatusBar translucent={true} backgroundColor={'transparent'} />
          <RadialGradient
          style={styles.container}
          colors={['#43cea2','#185a9d']}
          stops={[0.1,0.4,0.3,0.75]}
          center={[200,300]}
          radius={800}
          >
            <Thumbnail
              square
              style={styles.hhHand}
              source={require('../../img/HH-handwritten.png')}
            />
            <Text style={styles.H2}>Sign up as a Helper</Text>
            <Item  
              underline
              style={styles.input}
            >
              <Label>Email</Label>
              <Input
                autoCapitalize="none"
                autoCorrect={false}
                value={this.state.email}
                onChangeText={email => this.setState({ email })}
                keyboardType="email-address"
              />
            </Item>
            <Item
              underline
              style={styles.input}
            >
              <Label>Password</Label>
              <Input
                autoCapitalize="none"
                autoCorrect={false}
                secureTextEntry
                value={this.state.password}
                onChangeText={password => this.setState({ password })}
              />  
            </Item>

            <Button 
              onPress={this.handleSignUp} 
              style={styles.button}>
              <Text>Sign up</Text>
            </Button>
            
            <Button
              style={styles.button}
              onPress={() => this.props.navigation.navigate('Login')}>
              <Text>Already a helper? Login here</Text>
            </Button>

            
        
          </RadialGradient>
        </Container>
      </StyleProvider>
      
    )
  }
}
