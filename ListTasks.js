// Main.js

import React from 'react'
import { ActivityIndicator, StyleSheet, View, Flatlist, StatusBar, FlatList } from 'react-native'

import firebase from 'react-native-firebase'

import styles  from './../Styles/styles';

import {  Container, Header, Title, Left, Icon, Right, Button, Body, Content, Text, List, ListItem, Thumbnail, StyleProvider, Form, Item, Input, Label} from "native-base";
import getTheme from './../../native-base-theme/components';
import material from './../../native-base-theme/variables/material';
import RadialGradient from 'react-native-radial-gradient';

export default class ListTasks extends React.Component {
    constructor() {
        super();
        this.ref = firebase.firestore().collection('boards');
        this.unsubscribe = null;
        this.state = {
          isLoading: true,
          boards: []
        };
    }

    onCollectionUpdate = (querySnapshot) => {
        const boards = [];
        querySnapshot.forEach((doc) => {
            const { title, description, author } = doc.data();
            boards.push({
            key: doc.id,
            doc, // DocumentSnapshot
            title,
            description,
            author,
            });
        });

        this.setState({
            boards,
            isLoading: false,
        });
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    }

    _renderItem = ({ item }) => {
      
        return (
          <ListItem  style={styles.space}>
            <Left>
              <Text>{item.author} / {item.title}</Text>
            </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
          </ListItem>
        );
      }
  
render() {
    if(this.state.isLoading){
        return(
          <View>
            <ActivityIndicator size="large" color="#0000ff"/>
          </View>
        )
      }

return (
    <StyleProvider style={getTheme(material)}>
       <Container>
        <FlatList
          data={this.state.boards}
          renderItem={this._renderItem}
          keyExtractor={item => item.author}
        />
      </Container>
      </StyleProvider>
    )
  }
}

