// Main.js

import React from 'react'
import { StyleSheet, View, Flatlist, StatusBar } from 'react-native'

import firebase from 'react-native-firebase'

import styles  from './../Styles/styles';

import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Text, List, ListItem, Thumbnail, StyleProvider, Form, Item, Input, Label} from "native-base";
import getTheme from './../../native-base-theme/components';
import material from './../../native-base-theme/variables/material';
import RadialGradient from 'react-native-radial-gradient';
import ListTasks from './ListTasks';
import TabN


export default class Main extends React.Component {
  constructor() {
    super();
    this.ref = firebase.firestore().collection('boards');
    this.unsubscribe = null;
    this.state = {
      isLoading: true,
      boards: []
    };
  }

  state = { 
    currentUser: null,
    
  }

  componentDidMount() {
    const { currentUser } = firebase.auth()
    this.setState({ currentUser })
}

render() {
    const { currentUser } = this.state

return (
    <StyleProvider style={getTheme(material)}>
       <Container>
       <StatusBar translucent={true} backgroundColor={'transparent'} />
       <RadialGradient
      
          colors={['#43cea2','#185a9d']}
          stops={[0.1,0.4,0.3,0.75]}
          center={[-50,50]}
          radius={800}
          >
          <Header
       transparent 
        // style={styles.header}
       androidStatusBarColor='transparent'
      >
        <Left style={{flex: 1}}>
          <Button 
            style={{width: 65}}
            transparent
            onPress={() => firebase.auth().signOut()}
            >
              {/* {this.state.currentUser && (<Icon name='power-settings-new' style={styles.icon}  />)} */}
          </Button>
        </Left>

        <Body style={{flex: 3,justifyContent: 'center'}}>
          {/* <Title style={styles.headerText} >Helpers High</Title> */}
          <Thumbnail
              square
              style={styles.logo}
              source={require('../../img/logo-test.png')}
            />
        </Body>
        <Right style={{flex: 1}}>
          <Button 
            style={{width: 65}}
            transparent
            onPress={() => firebase.auth().signOut()}
            >
              {this.state.currentUser && (<Icon name='power-settings-new' style={styles.icon}  />)}
          </Button>
        </Right>
        </Header>

       
        </RadialGradient>
        
        <Text style={styles.welcomeText}>
          Hi {currentUser && currentUser.email}
        </Text>
        {/* <Button
              style={styles.button}
              onPress={() => this.props.navigation.navigate('ListTasks')}>
              <Text>List</Text>
            </Button> */}
            
        <ListTasks />  
        
        
      </Container>
      </StyleProvider>
    )
  }
}

